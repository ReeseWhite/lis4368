
# LIS4368

## Reese White

### Assignment 4 Requirements:

*Requirements:*

1. Data Validation
2. Compiling Servlet
3. Skillsets 10-12

#### README.md file should include the following items:

* Screenshots of Failed Validation
* Screenshot of Passed Validation
* Screenshots of skillsets 10-12

#### Assignment Screenshots:

*Failed Validation:*

![Failed Validation](img/a4a.png)

*Passed Validation:*

![Passed Validation](img/a4b.png)

#### Skillset Screenshots:

|Skill Set 10: Simple Calculator|Skill Set 11: Compound Interest Calculator|Skill Set 12: Array Copy|
|:-:|:-:|:-:|
|![Skill Set 10](img/ss10.png)|![SKill Set 11](img/ss11.png)|![Skill Set 12](img/ss12.png)|
