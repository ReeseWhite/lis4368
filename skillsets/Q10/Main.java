import java.util.Scanner;

class Main 
{
    public static void main(String args[])
  {
    //call static methods (i.e., no object)
    Methods.getRequirements();
    Methods.calculateNumbers();
  }
}
