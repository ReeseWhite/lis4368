> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Reese White

### Assignment 2 Requirements:

*Three Parts:*

1. Creating a Webservlet
2. Running Database Servlet
3. Java skillsets 1-3

#### README.md file should include the following items:

* Assesment links for localhost
* Screenshots of querybook.html
* Screenshots of query results
* Screenshots of a2 index


#### Assesment Links:

* [Directory test](http://localhost:9999/hello)
* [index.html](http://localhost:9999/hello)
* [Invokes HelloServlet](http://localhost:9999/hello/sayhello)
* [querybook](http://localhost:9999/hello/querybook.html)
>

#### Assignment Screenshots:

*Screenshot of HelloServlet*:

![servlet](img/a2a.png)

*Screenshot of Database Servlet*:

|querybook.html|Query Results|
|:-:|:-:|
|![querybook](img/a2b.png)|![Query Results](img/a2c.png)|

*Screenshot of a2 Index*:
![index](img/indexa2.png)

*Screenshots of Java SKill Sets*:

|Skill Set 1: System Properties|Skill Set 2: Looping Structures|Skill Set 3: Number Swap|
|:-:|:-:|:-:|
|![Skill Set 1](img/ss1.png)|![SKill Set 2](img/ss2.png)|![Skill Set 3](img/ss3.png)|
