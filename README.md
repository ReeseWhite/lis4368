> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368 - Advanced Web Applications Development 

## Reese White

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Provide git command descriptions
    - Complete Bitbucket tutorials
2. [A2 README.md](a2/README.md "My A2 README.md file") 
    - Creating Webpage servlet
    - Running database servlet
    - Java skillsets
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create Entry Relationship Diagram (ERD)
    - Create 10 entries for each attribute
    - Forward engineer script
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create Servlet 
    - Show passed and failed Data Validation 
    - Skillsets 10-12
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Utilize A4 server side data validation
    - Connect database to servlet
    - Complete CH. 13-15 questions
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create Client side validation
    - Chapter 9,10 ?'s
    - Java Skillsets
7. [P2 README.md](p2/README.md "My P2 README.md file")  
    - Use client and server side validation for entries
    - Create CRUD functionality
    - Use prepare statements to prevent SQL injection
    - Chapter 16,17 ?'s
