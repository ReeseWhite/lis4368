<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Reese White">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> This assignment built off of assignment 2 in relation to creating a mobile app with an image and a functioning button. This time also including a drop down array in order to select a band to calculate ticket prices. The button was made to run the calculation and then output the price for x amount of tickets to whichever band was selected from the drop down array. This assignment aslo included a database requirement which included an ERD that contained 3 tables with at least 10 entires each. 
				</p>

				<h4>First UI</h4>
				<img src="img/a3a.png" class="img-responsive center-block" alt="First UI">

				<h4>Second UI</h4>
				<img src="img/a3b.png" class="img-responsive center-block" alt="Second UI">

				<h4>ERD</h4>
				<img src="img/erd.png" class="img-responsive center-block" alt="ERD">

				<h4>Skillset 4</h4>
				<img src="img/ss4.png" class="img-responsive center-block" alt="Skillset 4">

				<h4>Skillset 5</h4>
				<img src="img/ss5.png" class="img-responsive center-block" alt="Skillset 5">

				<h4>Skillset 6</h4>
				<img src="img/ss6.png" class="img-responsive center-block" alt="Skillset 6">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
