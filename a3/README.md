> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Reese White

### Assignment 3 Requirements:

*Three Parts:*

1. Create ERD
2. Database with 3 tables and 10 unique entries
3. Forward Engineer sql script

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshots of 10 records for each table
* MWB file and SQL file

#### Assignment Screenshots:

*Screenshot of ERD:*

![ERD](img/erd.png)

*Screenshots of Table Entries:*

![Pet Store Table](img/a3c.png)|![Pet Table](img/a3d.png)|![Customer Table](img/a3e.png)|

*Screenshot of a3 index:*

![index](img/a3index.png)

#### mySQL Links:

*mySQL link:*
[mySQL](docs/a3.sql)

*mySQL MWB link:*
[MWB](docs/a3.mwb)


*Screenshots of Java SKill Sets*:

|Skill Set 4: Directory Info|Skill Set 5: Character Info|Skill Set 6: Determine Character|
|:-:|:-:|:-:|
|![Skill Set 4](img/ss4.png)|![SKill Set 5](img/ss5.png)|![Skill Set 6](img/ss6.png)|
