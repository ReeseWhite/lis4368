> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Reese White

### Assignment 1 Requirements:

*Three Parts:*

1. Distrubuted Versions Control with Git and Bitbucket
2. Java/JSP/Server Development Installations
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running Java Hello
* Screenshot of running http://localhost:9999
* Screenshot of a1/index.jsp
* git commands with short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorials (bitbucketstationlocations)

> 
>
>
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add . - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git notes - Add or inspect object notes

#### Assignment Screenshots:

*Screenshot of running Java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Tomcat running localhost:9999*:

![Tomcat Installation Screenshot](img/tomcat.png)

*Screenshot of a1.index.jsp*:

![a1 index Screenshot](img/index.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ReeseWhite/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

x