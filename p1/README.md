> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 

## Reese White

### Project 1 Requirements:

*Three Parts:*

1. Create Client Side validation
2. Skillsets 7-9
3. Chapter ?'s (9,10)

#### README.md file should include the following items:

* Screenshots of main page
* Screenshots of failed client side validation
* Screenshots of passed client side validation
* Screenshots of skillsets

#### Homepage index:

![index](img/p1a.png)

#### Client Side Validation:

![failed](img/p1b.png)
![passed](img/p1c.png)

#### Skillset Screenshots:

|Skill Set 8: ASCII part1|Skill Set 8: part 2|
|:-:|:-:|
|![SKill Set 8](img/ss8a.png)|![Skill Set 9](img/ss8b.png)|

|Skill Set 7: Count Characters|Skill Set 9: Grade Calculator|
|:-:|:-:|
|![skill set 7](img/ss7.png)|![failed](img/ss9.png)| 
