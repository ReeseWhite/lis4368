# LIS4368

## Reese White

### Project 2 Requirements:

*Three Parts:*

1. Add CRUD functionality
2. Client and Server side validation
3. Used prepared statements to prevent SQL injection

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry
* Screenshot of Passed Validation
* Screenshot of Displayed Data
* Screenshot of Modify Form
* Screenshot of Modified Data
* Screenshot of Delete Warning
* Screenshot of Associated Database Changes
>
>
>
>
>

#### Assignment Screenshots:

|Valid User Form Entry|Passed Validation|
|:-:|:-:|
|![Form entry](img/p2a.png)|![Passed Validation](img/p2b.png)|

*Screenshot of Displayed Data*:

![Displayed Data](img/p2c.png)

*Screenshot of Modify Form*:

![Modify form](img/p2d.png)

*Screenshot of Modified Data*:

![Modified Data](img/p2e.png)

*Screenshot of Delete Warning*:

![Delete Warnings](img/p2f.png)

*Screenshot of Associated Database Changes*:

![Associated Database](img/p2g.png)