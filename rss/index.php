<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Using RSS Feeds</title>
</head>
<body>
<?php

$html = "";
$publisher = "IGN";
$url = "http://feeds.feedburner.com/ign/all";

$html .= '<h2>' . $publisher . '</h2>';
$html .= $url;

$rss = simplexml_load_file($url);
$count = 0;
$html .= '<ol>';
foreach($rss->channel->item as $item)
{
    $count++;
    if($count > 10)
    {
        break;
    }
    $html .= '<li><a href="'.htmlspecialchars($item->link).'">'.htmlspecialchars($item->title) . '</a><br />';
    $html .= htmlspecialchars($item->description) . '<br />';
    $html .= htmlspecialchars($item->pubDate) . '</li><br />';
}
$html .= '</ol>';

print $html;
?>
</body>
</html>